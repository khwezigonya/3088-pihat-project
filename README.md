# 3088 PiHat Project

The micro-HAT drives the motor that has a maximum of two hall effect sensors to control speed, direction, and position/distance of a maximum of two motors.  A micro-HAT will be attached to a raspberry pi.


# Add extra information
It can be used in any scenario where the user wants to control the speed, direction, or position of a motor for example, to drive the wheels of a small robotic vehicle or light manufacturing conveyor belt.  


# Instructions on how to use it
Program the pi to control the motor and connect the pi hat to the motor
